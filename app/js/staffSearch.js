var staffSearchApp = angular.module('mq', []);

staffSearchApp.directive('mqStaffSearch', function() {
    return {
    	restrict: 'E',
        templateUrl : 'app/view/staffSearch.html',
        scope: {},
     	controller: function ($scope) {
         	$scope.items = [
				{
		            name: 'Alex',
		            role: 'Developer',
		            office: 'Sydney'
		        },
		        {
		            name: 'Ben',
		            role: 'Developer',
		            office: 'Wogga'
		        },
		        {
		            name: 'Sam',
		            role: 'Teacher',
		            office: 'Sydney'
		        },
		        {
		            name: 'Steve',
		            role: 'Builder',
		            office: 'Melbourne'
		        },
		        {
		            name: 'Josh',
		            role: 'Driver',
		            office: 'Sydney'
		        },
		        {
		            name: 'Sarah',
		            role: 'Lawyer',
		            office: 'Brisbane'
		        }
			];
        }
    };
});


staffSearchApp.filter('staffSearch', function(){

	return function(staffs, staffSearchQuery){

		if(!staffSearchQuery){
			return staffs;
		}

		var result = [];
		var staffObj = { name:'', role:'', office:''};
		var staffSearchQueryList = staffSearchQuery.toLowerCase().split(' ');
		
		angular.forEach(staffSearchQueryList, function(query) {
				
			var keyValuePair = query.split(':');

			if (keyValuePair[0] === 'name') {
				staffObj.name = keyValuePair[1];
			} else if (keyValuePair[0] === 'role') {
				staffObj.role = keyValuePair[1];
			} else if (keyValuePair[0] === 'office') {
				staffObj.office = keyValuePair[1];
			}
		});

		angular.forEach(staffs, function(staff){

			var isMatch = true;

			if (isMatch === true && staffObj.name) {
				if (staff.name.toLowerCase().indexOf(staffObj.name) !== -1) {
					isMatch = true;
				} else {
					isMatch = false;
				}
			}
 
			if (isMatch === true && staffObj.role) {
				if (staff.role.toLowerCase().indexOf(staffObj.role) !== -1) {
					isMatch = true;
				} else {
					isMatch = false;
				}
			}

			if (isMatch === true && staffObj.office) {
				if (staff.office.toLowerCase().indexOf(staffObj.office) !== -1) {
					isMatch = true;
				} else {
					isMatch = false;
				}
			}
 
			if (isMatch) {
				result.push(staff);
			}
		});

		return result;
	};
});
 
