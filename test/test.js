describe('staff search', function() {

	beforeEach(module('mq'));
	beforeEach(module('app/view/staffSearch.html'));

	var $scope, $compile, element, $filter;

	beforeEach(inject(function($rootScope, _$compile_) {
		$compile = _$compile_;
		$scope = $rootScope.$new();
		element = $compile('<mq-staff-search></mq-staff-search>')($scope);
	}));

	beforeEach(function () {
	    inject(function (_$filter_) {
      		$filter = _$filter_;
	    });
  	});

    it('should load staff data', function() {
        var data = element.isolateScope().data;
        expect(data.length).toEqual(6);
    });

	it('should filter by name return correct item number', function() {
		var result = $filter('search')('name:alex');
	 	expect(result.length).toEqual(1);
    });

    it('should filter by name return correct object', function() {
		var result = $filter('search')('name:alex');
	 	expect(result).toEqual([{name: 'Alex', role: 'Developer', office: 'Sydney'}]);
    });

     it('should filter by role return correct object', function() {
		var result = $filter('search')('role:teacher');
	 	expect(result).toEqual([{name: 'Sam', role: 'Teacher', office: 'Sydney'}]);
    });
});